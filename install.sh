#!/bin/bash

PWD=`pwd`

#install requirements
sudo apt-get -y install i3 i3blocks yad xdotool scrot gimp fonts-font-awesome i3lock-fancy imagemagick numlockx

#battery indicator
echo "Show battery indicator?:"
echo "  1. YES"
echo "  2. NO"
read -p "Your choice: " opt

case $opt in
    1)
        sudo ln -sf $PWD/configs/i3blocks-with-battery.conf /etc/i3blocks.conf
        ;;
    2)
        sudo ln -sf $PWD/configs/i3blocks-no-battery.conf /etc/i3blocks.conf
        ;;
    *)
        echo "Undefined option. Exit installer"
        exit
        ;;
esac

#create i3 config dir
mkdir $HOME/.config/i3

#create symbolic links
ln -sf $PWD/configs/config $HOME/.config/i3/.
ln -sf $PWD/scripts/* $HOME/.config/i3/.
ln -sf $PWD/images/* $HOME/.config/i3/.

#restart system
sudo reboot
